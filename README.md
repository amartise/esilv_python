# ESILV_python

Repository for ESILV students at Dorset College Dublin - Spring term 2022

Find the basic GIT commands in the following link:
https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html

## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/amartise/esilv_python.git
git branch -M main
git push -uf origin main
```

## Author
Alberto Martinez (Dorset College Dublin)
